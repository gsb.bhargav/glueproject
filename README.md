# Usage:
```hcl
module "gluejob" {
  source = "../modules/glue"

## IAM Policy
gluerole_name       = "AWSGlueServiceRoleDefault"

## IAM Policy for S3 Bucket
s3policy_name       = "my_s3_policy"

## Glue Job details
gluejob_name        = "BHSF Glue Job"    
gluejob_description = "Glue job for BHSF"
glue_version        = "2.0"
glue_workertype     = "G.1X"
glue_numberofworkers= "10"
maximumretries      = "0"
timeout             = "2880"
scriptpath          = "s3://mygluejobproject/src/gluejob.py"
pyversion           = "3"
joblanguage         = "python"
jobbookmarkoption   = "job-bookmark-disable"
tempdir             = "s3://aws-glue-temporary-697417170774-us-east-1/admin"
extrapyfiles        = "s3://dw-src-level-1/src/code/mvp-project.zip,s3://dw-src-level-1/src/code/job.properties"
}

```
## Features:

This terraform code aims to create the below on AWS services.
1. Creates Glue Job using the varibale details as provided above.
